package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task add(Task task);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    void clear();

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
