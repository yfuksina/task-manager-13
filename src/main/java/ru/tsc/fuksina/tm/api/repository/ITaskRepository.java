package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.Task;
import ru.tsc.fuksina.tm.service.TaskService;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task add(Task task);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    void clear();

}
