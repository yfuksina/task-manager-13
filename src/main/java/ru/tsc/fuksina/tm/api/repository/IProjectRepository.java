package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    boolean existsById(String id);

    Project remove(Project project);

    Project removeByIndex(Integer index);

    Project removeById(String id);

    void clear();

    List<Project> findAll();

}
